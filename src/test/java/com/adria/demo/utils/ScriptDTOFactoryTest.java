package com.adria.demo.utils;

import com.adria.demo.models.ScriptDTO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScriptDTOFactoryTest {

    @Test
    void getInstance() {
        String missplacedScriptPath="postgres/Data/18319/0168_CREATE_VIREMENT_INTERNATIONNAL_PERMANENT.sql";
        ScriptDTOFactory scriptDTOFactory=new ScriptDTOFactory();
        ScriptDTO scriptDTO=scriptDTOFactory.getInstance(missplacedScriptPath);
        assertEquals('D',scriptDTO.getType());

    }

}