package com.adria.demo.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScriptStructureDTOTest {

    @Test
    void isMisplaced() {
        String path = "postgres/Structure/0001_ALTER_TEST.sql";
        ScriptStructureDTO scriptStructureDTO = new ScriptStructureDTO(path);

        assertFalse(scriptStructureDTO.isMisplaced());
    }
}