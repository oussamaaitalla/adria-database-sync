package com.adria.demo.models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScriptDataDTOTest {

    @Test
    void isMisplaced() {
        String path = "postgres/Data/10001/0001_ALTER_TEST.sql";
        ScriptDataDTO scriptStructureDTO = new ScriptDataDTO(path);

        assertTrue(scriptStructureDTO.isMisplaced());
    }
}