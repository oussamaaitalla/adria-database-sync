package com.adria.demo.controllers;


import com.adria.demo.configuration.BitbucketConfig;
import com.adria.demo.models.PullRequestDTO;
import com.adria.demo.models.ScriptDTO;
import com.adria.demo.service.DatabaseServiceStrategy;
import com.adria.demo.service.DatabaseServiceFactory;
import com.adria.demo.service.PullRequestService;
import com.adria.demo.constants.Constantes;
import com.adria.demo.utils.ScriptHandler;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * WebhookListener is a restcontroller class that intercepts bitbucket webhook requests
 */
@RestController
public class WebhookListener {
    private PullRequestService pullRequestService;
    private DatabaseServiceFactory databaseServiceFactory;
    private BitbucketConfig bitbucketConfig;
    private ScriptHandler scriptHandler;
    private Logger logger= LoggerFactory.getLogger(WebhookListener.class);


    public WebhookListener(DatabaseServiceFactory databaseServiceFactory,PullRequestService pullRequestService, BitbucketConfig bitbucketConfig, ScriptHandler scriptHandler) {
        this.bitbucketConfig = bitbucketConfig;
        this.scriptHandler = scriptHandler;
        this.pullRequestService=pullRequestService;
        this.databaseServiceFactory=databaseServiceFactory;
    }

    /**
     * this metheod is used to intercept bitbucket http post requests on each pullrequest merge or creation event.
     * if the event is a pullrequest merge , the method launches the synchronization of the databases
     * if the event is a pullrequest creation , the method launches the conformity validation of the PR's scripts.
     * @param eventKey this is  the type of the triggred event from bitbucket webhook
     * @param hookUuid this is a unique ID for the webhook
     * @param requestBody this is the body of the intercepted Post request
     * @throws Exception
     * @throws URISyntaxException
     * @throws IOException
     * @throws SQLException
     */
    @PostMapping("/bitbucket-webhook")
    public void bitbucketWebhookListening(@RequestHeader("X-Event-Key") String eventKey,@RequestHeader("X-Hook-UUID") String hookUuid, @RequestBody String requestBody) throws Exception,URISyntaxException, IOException, SQLException {
        if(hookUuid!=null && hookUuid.equals(bitbucketConfig.getHookUuid()) && requestBody!=null){
            PullRequestDTO pullRequestDTO=new PullRequestDTO(requestBody);
            String scriptsPathsPayload = pullRequestService.getScriptsPathsPayload(pullRequestDTO.getPullRequestScriptsPathsUrl());
            Map<String, List<ScriptDTO>> newScriptsPaths = pullRequestService.extractScriptsPathFromPaylaod(scriptsPathsPayload);
            pullRequestDTO.setScriptsByDBType(newScriptsPaths);
            pullRequestDTO.setPRScriptsType();

            if(eventKey!=null && eventKey.equals(bitbucketConfig.getPr_merge_event())){
                logger.info("New Pull Request has been Merged!");
                logger.info("Create Scripts directory tree");
                scriptHandler.createDirectoryTree(newScriptsPaths);
                logger.info("Checking if the databases are synchronized ...");

                DatabaseServiceStrategy databaseServiceStrategy =databaseServiceFactory.getDatabaseService(pullRequestDTO.getScriptsType());
                databaseServiceStrategy.synchronizeAllDatabases(pullRequestDTO.getScriptsByDBType());

            }
            if(eventKey!=null && eventKey.equals(bitbucketConfig.getPr_created_event())){
                logger.info("New Pull Request has been created!");
                scriptHandler.verifyPullRequestScriptsConformity(pullRequestDTO);
            }


        }
        else logger.error("Acess denied !!!");

    }




}
