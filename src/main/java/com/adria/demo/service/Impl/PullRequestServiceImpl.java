package com.adria.demo.service.Impl;

import com.adria.demo.configuration.BitbucketConfig;
import com.adria.demo.configuration.BitbucketOauthConfig;
import com.adria.demo.constants.Constantes;
import com.adria.demo.models.RejectedScript;
import com.adria.demo.models.ScriptDTO;
import com.adria.demo.models.MessageDTO;
import com.adria.demo.service.AuthService;
import com.adria.demo.service.PullRequestService;
import com.adria.demo.utils.ScriptDTOFactory;
import com.adria.demo.utils.ScriptHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;


@Service
public class PullRequestServiceImpl implements PullRequestService {

    AuthService authService;
    BitbucketOauthConfig bitbucketOauthConfig;
    BitbucketConfig bitbucketConfig;
    Logger logger= LoggerFactory.getLogger(PullRequestServiceImpl.class);


    PullRequestServiceImpl(AuthService authService, BitbucketOauthConfig bitbucketOauthConfig, BitbucketConfig bitbucketConfig){
        this.authService=authService;
        this.bitbucketOauthConfig=bitbucketOauthConfig;
        this.bitbucketConfig=bitbucketConfig;
    }



    public Map<String, List<ScriptDTO>> extractScriptsPathFromPaylaod(String payload) throws URISyntaxException {
        Map<String, List<ScriptDTO>> paths = new HashMap<>();
        if(payload!=null){
            JSONObject jsonObject=new JSONObject(payload);
            JSONArray filesPayload=jsonObject.getJSONArray("values");
            int size=filesPayload.length();
            for(int i=0;i<size;i++){
                String fileStatus=filesPayload.getJSONObject(i).getString("status");
                //TODO : à revoir de tel sorte si on change la RG
                if(fileStatus.equals("added")||fileStatus.equals("modified")) {
                    String path = filesPayload.getJSONObject(i).getJSONObject("new").getString("path").trim();
                    String scriptLink=filesPayload.getJSONObject(i).getJSONObject("new").getJSONObject("links").getJSONObject("self").getString("href");
                    ScriptDTOFactory scriptDTOFactory=new ScriptDTOFactory();
                    ScriptDTO scriptDTO =scriptDTOFactory.getInstance(path);
                    scriptDTO.setStatus(fileStatus);
                    scriptDTO.setLink(scriptLink);
                    String dbType=scriptDTO.getPlatform();
                    if(paths.keySet().contains(dbType)) {
                        paths.get(dbType).add(scriptDTO);
                    }
                    else {
                        List<ScriptDTO> scriptDTOs=new ArrayList<>();
                        scriptDTOs.add(scriptDTO);
                        paths.put(dbType,scriptDTOs);
                    }
                }
            }
            return paths;
        }
        return null;
    }

    public String getScriptsPathsPayload(String scriptsPathsPayloadUrl) throws URISyntaxException {
        return getBodyFromHttpResponse(scriptsPathsPayloadUrl);

    }

    public String getBodyFromHttpResponse(String url) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response;
        String token=bitbucketOauthConfig.getAccess_token();
        if(token.equals("")){
            authService.setAccessToken(bitbucketOauthConfig);
            token=bitbucketOauthConfig.getAccess_token();
        }
        response = getHttpResponse(url, restTemplate, token);
        if(response!=null){
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody();
            }
            if (response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                authService.setAccessTokenAfterExpires(bitbucketOauthConfig);
                token = bitbucketOauthConfig.getAccess_token();
                response = getHttpResponse(url, restTemplate, token);
                if (response.getStatusCode() == HttpStatus.OK) {
                    return response.getBody();
                }

            }
        }
        return null;
    }

    private ResponseEntity<String> getHttpResponse(String url, RestTemplate restTemplate, String token) throws URISyntaxException {
        HttpHeaders httpHeaders=createHeaders(token);
        ResponseEntity<String> response=null;
        HttpEntity<?> entity = new HttpEntity<>(httpHeaders);
        URI uri=new URI(url);
        try {
            response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        }catch (HttpClientErrorException e){
            logger.error(""+e.getStatusCode());
        }
        return response;
    }

    public HttpHeaders createHeaders(String token){
        return new HttpHeaders() {{
            String authHeader = "Bearer " + token;
            set( "Authorization", authHeader );
        }};
    }


    @Override
    public void approvePullRequest(int prId) {
        logger.info("Approving pull request with id : "+prId+" ...");
        String url=bitbucketConfig.getApi_base_url()+bitbucketConfig.getWorkspace()+"/"+bitbucketConfig.getRepositoryName()+"/pullrequests/"+prId+"/approve";
        HttpClient httpClient= HttpClients.createDefault();
        MessageDTO message=new MessageDTO(Constantes.PR_APPROVED_MSG);
        String action = "approved";
        prActionHttpRequest(url,httpClient,message,prId, action);
    }

    private void prActionHttpRequest(String url, HttpClient httpClient, MessageDTO message, int prId, String action) {
        String token=bitbucketOauthConfig.getAccess_token();
        if (token.equals("")) authService.setAccessToken(bitbucketOauthConfig);

        HttpPost httpPost;
        HttpResponse response;

        try {
            httpPost = prepareRequest(url, token, message);
            response = httpClient.execute(httpPost);
            checkResponseStatusCode(url, httpClient, message, prId, response, action);
        } catch (IOException e) {
            //e.printStackTrace();
            logger.error(e.getMessage());
        }


    }


    @Override
    public void declinePullRequest(Set<RejectedScript> rejectedScripts, int pullRequestId) throws Exception {
        logger.info("Declining pull request with id : "+pullRequestId+" ...");
        String url=bitbucketConfig.getApi_base_url()+bitbucketConfig.getWorkspace()+"/"+bitbucketConfig.getRepositoryName()+"/pullrequests/"+pullRequestId+"/decline";
        HttpClient httpClient= HttpClients.createDefault();
        MessageDTO message=new MessageDTO(rejectedScripts);
        String action = "declined";
        prActionHttpRequest(url,httpClient,message,pullRequestId, action);
    }



    private HttpPost  prepareRequest(String url,String token,MessageDTO message) {
        HttpPost httpPost=new HttpPost(url);

        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        ObjectMapper mapper = new ObjectMapper();
        StringEntity stringEntity= null;
        try {
            stringEntity = new StringEntity(mapper.writeValueAsString(message));
        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            //e.printStackTrace();
            logger.error(e.getMessage());
        }
        httpPost.setEntity(stringEntity);
        return httpPost;
    }


    private void checkResponseStatusCode(String url, HttpClient httpClient, MessageDTO message, int pullRequestId, HttpResponse response, String action) {
        String token;
        HttpPost httpPost;
        if(response.getStatusLine().getStatusCode()==200) {
            logger.info("PullRequest with id :  "+pullRequestId+" is "+action);
        }
        else if(response.getStatusLine().getStatusCode()== 401){
            authService.setAccessTokenAfterExpires(bitbucketOauthConfig);
            token=bitbucketOauthConfig.getAccess_token();
            httpPost=prepareRequest(url,token,message);
            try {
                response = httpClient.execute(httpPost);
            } catch (IOException e) {
                //e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
        else {
            logger.warn("Code "+response.getStatusLine().getStatusCode()+" : Pull request was not "+action);
        }
    }
}
