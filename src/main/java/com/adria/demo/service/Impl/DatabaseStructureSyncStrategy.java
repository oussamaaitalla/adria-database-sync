package com.adria.demo.service.Impl;

import com.adria.demo.configuration.DatabaseConfig;
import com.adria.demo.models.ScriptDTO;
import com.adria.demo.models.SuiviRecord;
import com.adria.demo.models.SyncStatus;
import com.adria.demo.service.DatabaseServiceStrategy;
import com.adria.demo.utils.BitbucketHelper;
import com.adria.demo.utils.DatabaseHelper;
import com.adria.demo.utils.ScriptHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.List;
import java.util.Map;

@Service
public class DatabaseStructureSyncStrategy extends DatabaseHelper implements DatabaseServiceStrategy {
    private DatabaseConfig databaseConfig;
    private ScriptHandler scriptHandler;
    private BitbucketHelper bitbucketHelper;


    private Logger logger= LoggerFactory.getLogger(DatabaseStructureSyncStrategy.class);

    public DatabaseStructureSyncStrategy(BitbucketHelper bitbucketHelper, DatabaseConfig databaseConfig, ScriptHandler scriptHandler){
        super(databaseConfig);
        this.bitbucketHelper=bitbucketHelper;
        this.databaseConfig = databaseConfig;
        this.scriptHandler = scriptHandler;
    }



    @Override
    public SyncStatus getDBSyncStatus(Connection connection,Map<String, List<ScriptDTO>> newScriptsPaths, String dbType) {
        SyncStatus syncStatus = new SyncStatus();
        if(newScriptsPaths.get(dbType).isEmpty()) {
            logger.warn("[EMPTY] : There is no scripts with database type ["+dbType+"] in this PR!");
            return null;
        }
        else {
            List<SuiviRecord> records = getLatestNLoggedScripts(newScriptsPaths.get(dbType).size(), connection, dbType,'S');
            for(ScriptDTO scriptDTO: newScriptsPaths.get(dbType)) {
                boolean isExist = false;
                for(SuiviRecord record: records) {
                    if(scriptDTO.getName().equals(record.getNom())) {
                        logger.warn("Script ==" + scriptDTO.getName() + "== already exists in the logging table!");
                        isExist = true;
                        break;
                    }
                    else {
                        isExist = false;
                    }
                }
                if(!isExist) {
                    logger.warn("Script ==" + scriptDTO.getName() + "== does not exist in the logging table!");
                    syncStatus.getNotSyncScripts().add(scriptDTO);
                }
            }

            syncStatus.checkDatabaseSyncStatus();
        }

        return syncStatus;

    }






    @Override
    public void synchronizeAllDatabases(Map<String, List<ScriptDTO>> prScriptsPaths) throws SQLException, URISyntaxException, IOException {
        List<DataSourceProperties> dataSources=databaseConfig.getDatasources();
        for(DataSourceProperties dataSource:dataSources){
            String dbUrl = dataSource.getUrl();
            String dbUsername = dataSource.getUsername();
            String dbPassword = dataSource.getPassword();
            String codeBanque = dataSource.getName();
            String dbType = dataSource.getPlatform();

            logger.info("======== Current selected DATABASE : " + codeBanque);
            Connection connection = getDatabaseConnection(dbUrl, dbUsername, dbPassword);
            if(connection!=null){
                SyncStatus syncStatus = getDBSyncStatus(connection, prScriptsPaths, dbType);
                boolean synchronizationStatus=isDatabaseSynchronized(syncStatus,codeBanque);
                if(!synchronizationStatus){
                    logger.info("Sync IN PROGRESS ...");
                    synchronizeDatabase(connection, syncStatus.getNotSyncScripts(), codeBanque);
                }
                closeDatabaseConnection(connection);
            }
            else {
                logger.warn("database connection failed ..");
            }
        }
        scriptHandler.deleteDownloadedScripts();
    }

    public void synchronizeDatabase(Connection connection, List<ScriptDTO> scriptDTOList, String codeBanque) throws URISyntaxException, SQLException, IOException {
        logger.info("NEEDED Scripts to be executed :");

        scriptDTOList.forEach(scriptDTO -> System.out.println("--> "+scriptDTO.getRelativeRepoPath()));


        logger.info("Downloading new added Scripts ....");
        bitbucketHelper.downloadNewAddedScripts(scriptDTOList);

        logger.info("Executing the missing scripts ...");
        executeMissingScripts(connection, scriptDTOList,codeBanque);
    }

    @Override
    public void executeMissingScripts(Connection connection, List<ScriptDTO> scriptDTOList, String codeBanque) throws SQLException, IOException {
        for(ScriptDTO scriptDTO: scriptDTOList) {
            logger.info("---> executing script with type [Structure] : " + scriptDTO.getLocalPath());
            executeScript(connection, scriptDTO);

        }
    }
}
