package com.adria.demo.service.Impl;

import com.adria.demo.configuration.BitbucketOauthConfig;
import com.adria.demo.constants.Constantes;
import com.adria.demo.service.AuthService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

@Service
public class AuthServiceImpl implements AuthService {


    @Override
    public void setAccessToken(BitbucketOauthConfig bitbucketOauthConfig) {
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
        body.add("grant_type", "client_credentials");
        HttpHeaders httpHeaders=createHeaders(bitbucketOauthConfig);
        HttpEntity<?> entity = new HttpEntity<Object>(body, httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(bitbucketOauthConfig.getEndpoint(), HttpMethod.POST, entity, String.class);

        if(response.getStatusCode()== HttpStatus.OK){
            String token=extractAccessToken(response);
            bitbucketOauthConfig.setAccess_token(token);
            String refresh_token=extractRefreshToken(response);
            bitbucketOauthConfig.setRefresh_token(refresh_token);
        }

    }

    @Override
    public void setAccessTokenAfterExpires(BitbucketOauthConfig bitbucketOauthConfig){
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
        body.add("grant_type", "refresh_token");
        body.add("grant_type", bitbucketOauthConfig.getRefresh_token());
        HttpHeaders httpHeaders=createHeaders(bitbucketOauthConfig);
        HttpEntity<?> entity = new HttpEntity<Object>(body, httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange(bitbucketOauthConfig.getEndpoint(), HttpMethod.POST, entity, String.class);

        if(response.getStatusCode()==HttpStatus.OK){
            String token=extractAccessToken(response);
            bitbucketOauthConfig.setAccess_token(token);
            String refresh_token=extractRefreshToken(response);
            bitbucketOauthConfig.setRefresh_token(refresh_token);
        }

    }



    private String extractAccessToken(ResponseEntity<String> response) {
        JSONObject jsonObj = new JSONObject(response.getBody());
        String  access_token = jsonObj.get("access_token").toString();
        return access_token;
    }

    private String extractRefreshToken(ResponseEntity<String> response){
        JSONObject jsonObj = new JSONObject(response.getBody());
        String  access_token = jsonObj.get("refresh_token").toString();
        return access_token;
    }

    public HttpHeaders createHeaders(BitbucketOauthConfig bitbucketOauthConfig){
        return new HttpHeaders() {{
            String auth = bitbucketOauthConfig.getKey() + ":" + bitbucketOauthConfig.getSecret();
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }


}
