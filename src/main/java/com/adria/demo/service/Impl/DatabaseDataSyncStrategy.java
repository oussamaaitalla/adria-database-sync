package com.adria.demo.service.Impl;

import com.adria.demo.configuration.DatabaseConfig;
import com.adria.demo.models.ScriptDTO;
import com.adria.demo.models.SuiviRecord;
import com.adria.demo.models.SyncStatus;
import com.adria.demo.service.DatabaseServiceStrategy;
import com.adria.demo.utils.BitbucketHelper;
import com.adria.demo.utils.DatabaseHelper;
import com.adria.demo.utils.ScriptHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DatabaseDataSyncStrategy extends DatabaseHelper implements DatabaseServiceStrategy {

    private BitbucketHelper bitbucketHelper;
    private DatabaseConfig databaseConfig;
    private ScriptHandler scriptHandler;

    private Logger logger= LoggerFactory.getLogger(DatabaseStructureSyncStrategy.class);

    public DatabaseDataSyncStrategy(BitbucketHelper bitbucketHelper, DatabaseConfig databaseConfig, ScriptHandler scriptHandler){
        super(databaseConfig);
        this.bitbucketHelper = bitbucketHelper;
        this.databaseConfig = databaseConfig;
        this.scriptHandler = scriptHandler;
    }



    @Override
    public SyncStatus getDBSyncStatus(Connection connection, Map<String, List<ScriptDTO>> newScriptsPaths, String dbType) {
        SyncStatus syncStatus = new SyncStatus();

        int numberOfScripts=newScriptsPaths.get(newScriptsPaths.keySet().toArray()[0]).size();

        List<SuiviRecord> records = getLatestNLoggedScripts(numberOfScripts, connection, dbType,'D');
        List<ScriptDTO> scripts=newScriptsPaths.get(newScriptsPaths.keySet().toArray()[0]);
        for(ScriptDTO scriptDTO: scripts) {
            boolean isExist = false;
            for(SuiviRecord record: records) {
                if(scriptDTO.getName().equals(record.getNom())) {
                    logger.warn("Script ==" + scriptDTO.getName() + "== already exists in the logging table!");
                    isExist = true;
                    break;
                }
                else {
                    isExist = false;
                }
            }
            if(!isExist) {
                logger.warn("Script ==" + scriptDTO.getName() + "== does not exist in the logging table!");
                syncStatus.getNotSyncScripts().add(scriptDTO);
            }
        }

        syncStatus.checkDatabaseSyncStatus();


        return syncStatus;

    }



    @Override
    public void synchronizeAllDatabases(Map<String, List<ScriptDTO>> prScriptsPaths) {
        Map<String,List<ScriptDTO>> scriptsPerCodeBank=splitScriptsbyBankCode1(prScriptsPaths);

        List<DataSourceProperties> dataSourceProperties=databaseConfig.getDatasources();

        dataSourceProperties.forEach((dataSource)->{
            if(scriptsPerCodeBank.containsKey(dataSource.getName())){
                String dbUrl = dataSource.getUrl();
                String dbUsername = dataSource.getUsername();
                String dbPassword = dataSource.getPassword();
                String codeBanque = dataSource.getName();
                String dbType = dataSource.getPlatform();

                logger.info("======== Current selected DATABASE : " + codeBanque);

                Connection connection = getDatabaseConnection(dbUrl, dbUsername, dbPassword);
                if(connection!=null){
                    Map<String,List<ScriptDTO>> scriptsPerOneCodeBank=new HashMap<>();
                    scriptsPerOneCodeBank.put(dataSource.getName(),scriptsPerCodeBank.get(dataSource.getName()));
                    SyncStatus syncStatus = getDBSyncStatus(connection, scriptsPerOneCodeBank, dbType);
                    boolean synchronizationStatus=isDatabaseSynchronized(syncStatus,codeBanque);
                    if(!synchronizationStatus){
                        logger.info("Sync IN PROGRESS ...");
                        try {
                            synchronizeDatabase(connection, syncStatus.getNotSyncScripts(), codeBanque);
                        } catch (URISyntaxException | SQLException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                    closeDatabaseConnection(connection);
                }
                else{
                    logger.warn("database connection failed ..");

                }
            }
        });
        scriptHandler.deleteDownloadedScripts();
    }

    public void synchronizeDatabase(Connection connection, List<ScriptDTO> scriptDTOList, String codeBanque) throws URISyntaxException, SQLException, IOException {
        logger.info("NEEDED Scripts to be executed :");

        scriptDTOList.forEach(scriptDTO -> System.out.println("--> "+scriptDTO.getRelativeRepoPath()));


        logger.info("Downloading new added Scripts ....");
        bitbucketHelper.downloadNewAddedScripts(scriptDTOList);

        logger.info("Executing the missing scripts ...");
        executeMissingScripts(connection, scriptDTOList,codeBanque);
    }

    @Override
    public void executeMissingScripts(Connection connection, List<ScriptDTO> scriptDTOList, String codeBanque) throws SQLException, IOException {
        for(ScriptDTO scriptDTO: scriptDTOList) {
            if(scriptDTO.getCodeBanque().equals(codeBanque)) {
                logger.info("---> executing script with type [DATA] : " + scriptDTO.getLocalPath());
                logger.info("---> target database : " + codeBanque);
                executeScript(connection, scriptDTO);
            }
        }
    }


    private Map<String, List<ScriptDTO>> splitScriptsbyBankCode1(Map<String, List<ScriptDTO>> prScriptsPaths){

        List<ScriptDTO> scripts=prScriptsPaths.entrySet()
                .stream()
                .flatMap((scriptsDTO)->scriptsDTO.getValue().stream())
                .collect(Collectors.toList());

        Map<String,List<ScriptDTO>> scriptsPerCodeBank=new HashMap<>();
        scripts.forEach((scriptDTO -> {
            if(scriptsPerCodeBank.containsKey(scriptDTO.getCodeBanque())){
                scriptsPerCodeBank.get(scriptDTO.getCodeBanque()).add(scriptDTO);
            }else {
                List<ScriptDTO> scriptDTOs=new ArrayList<>();
                scriptDTOs.add(scriptDTO);
                scriptsPerCodeBank.put(scriptDTO.getCodeBanque(),scriptDTOs);
            }

        }));

        return scriptsPerCodeBank;

    }





}
