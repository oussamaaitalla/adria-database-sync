package com.adria.demo.service;

import com.adria.demo.service.Impl.DatabaseDataSyncStrategy;
import com.adria.demo.service.Impl.DatabaseStructureSyncStrategy;
import org.springframework.stereotype.Component;

@Component
public  class DatabaseServiceFactory {
    public DatabaseStructureSyncStrategy databaseStructureSync;
    public DatabaseDataSyncStrategy databaseDataSync;
    public DatabaseServiceFactory(DatabaseStructureSyncStrategy databaseStructureSync, DatabaseDataSyncStrategy databaseDataSync){
        this.databaseStructureSync = databaseStructureSync;
        this.databaseDataSync = databaseDataSync;
    }

    public DatabaseServiceStrategy getDatabaseService(String scriptType){
        if(scriptType.equals("S")){
            return databaseStructureSync;
        }
        else {
            return databaseDataSync;
        }
    }
}
