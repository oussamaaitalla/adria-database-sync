package com.adria.demo.service;

import com.adria.demo.models.RejectedScript;
import com.adria.demo.models.ScriptDTO;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Set;


public interface PullRequestService {


     Map<String, List<ScriptDTO>> extractScriptsPathFromPaylaod(String newAddedFilesPathsUrl) throws URISyntaxException;

     String getScriptsPathsPayload(String scriptsPathsPayloadUrl) throws URISyntaxException;

     HttpHeaders createHeaders(String token);

    void declinePullRequest(Set<RejectedScript> rejectedScript, int pullRequestId) throws Exception;
    String getBodyFromHttpResponse(String url) throws URISyntaxException;

    void approvePullRequest(int prId);
}
