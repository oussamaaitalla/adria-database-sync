package com.adria.demo.service;

import com.adria.demo.models.ScriptDTO;
import com.adria.demo.models.SuiviRecord;
import com.adria.demo.models.SyncStatus;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface DatabaseServiceStrategy {
    SyncStatus getDBSyncStatus(Connection connection, Map<String, List<ScriptDTO>> newScriptsPaths, String dbType);
    void synchronizeAllDatabases(Map<String, List<ScriptDTO>> newScriptsPaths) throws SQLException, URISyntaxException, IOException;
    void executeMissingScripts(Connection connection, List<ScriptDTO> scriptDTOList, String codeBanque) throws SQLException, IOException;
}