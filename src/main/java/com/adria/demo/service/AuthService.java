package com.adria.demo.service;

import com.adria.demo.configuration.BitbucketOauthConfig;
import org.springframework.http.HttpHeaders;


public interface AuthService {

    void setAccessToken(BitbucketOauthConfig bitbucketOauthConfig);
    void setAccessTokenAfterExpires(BitbucketOauthConfig bitbucketOauthConfig);
    HttpHeaders createHeaders(BitbucketOauthConfig bitbucketOauthConfig);
}
