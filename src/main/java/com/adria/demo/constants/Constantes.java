package com.adria.demo.constants;

public class Constantes {
    public static final String SCRIPTS_DIRECTORY_NAME="Scripts";

    public static final String MISSING_INSERT_TO_ADRIA_SUIVI_INSTRUCTION="does not contain insert to adria_database_suivi instruction \n";
    public static final String SCRIPT_NAME_LOG_INSTRUCTION_MISMATCH="script name does not match log instruction data inside the script";
    public static final String MISSING_DELETE_BEFORE_INSERT_INSTRUCTION="does not contain  delete before insert instruction";
    public static final String DUPLICATE_SCRIPT_NUMBER_DECLINE_MSG = "script number is already used in the repo";
    public static final String PR_IS_MISSING_SCRIPTS_FOR_ALL_DB_PLATFORM = "the PR does not handle all database platforms";
    public static final String PR_APPROVED_MSG = "All scripts in this created PR passed the conformity steps!";
    public static final String PR_DECLINE_MSG_MISPLACED_SCRIPT = "This script is misplaced based on its type";
}
