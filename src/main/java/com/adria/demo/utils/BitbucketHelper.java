package com.adria.demo.utils;

import com.adria.demo.configuration.BitbucketOauthConfig;
import com.adria.demo.models.ScriptDTO;
import com.adria.demo.service.AuthService;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@Component
public class BitbucketHelper {

    private BitbucketOauthConfig bitbucketOauthConfig;
    private AuthService authService;
    private ScriptHandler scriptsHandler;
    private Logger logger= LoggerFactory.getLogger(BitbucketHelper.class);

    public BitbucketHelper(BitbucketOauthConfig bitbucketOauthConfig, AuthService authService, ScriptHandler scriptsHandler) {
        this.bitbucketOauthConfig = bitbucketOauthConfig;
        this.authService = authService;
        this.scriptsHandler = scriptsHandler;
    }




    public void downloadNewAddedScripts(List<ScriptDTO> filesBitbucketPaths) throws URISyntaxException, IOException {
        for(ScriptDTO scriptDTO:filesBitbucketPaths){
            if(!scriptsHandler.isScriptAlreadyDownloaded(scriptDTO)){
                String fullBitbucketFileUrl=scriptsHandler.constructUrl(scriptDTO.getRelativeRepoPath().replaceAll("\\s+","%20"));
                logger.info("Download from --> "+fullBitbucketFileUrl);
                downloadScript(fullBitbucketFileUrl, scriptDTO);
            }

        }
    }

    public HttpResponse downloadScriptRequest(String url) throws URISyntaxException, IOException {
        HttpClient client = HttpClients.custom().build();
        HttpUriRequest request;
        HttpResponse response;
        String token=bitbucketOauthConfig.getAccess_token();
        if(token.equals("")) authService.setAccessToken(bitbucketOauthConfig);

        request = prepareScriptDownloadHttpRequest(url,token);

        response=client.execute(request);
        if(response.getStatusLine().getStatusCode()==200) return response;
        if(response.getStatusLine().getStatusCode()==401){
            authService.setAccessTokenAfterExpires(bitbucketOauthConfig);
            token=bitbucketOauthConfig.getAccess_token();
            request = prepareScriptDownloadHttpRequest(url,token);
            response=client.execute(request);
            if(response.getStatusLine().getStatusCode()==200) return response;
            else return null;

        }
        return null;

    }

    public HttpUriRequest prepareScriptDownloadHttpRequest(String url,String token) throws URISyntaxException {
        return RequestBuilder.get()
                .setUri(new URI(url))
                .setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .build();
    }
    public void downloadScript(String fullBitbucketFileUrl, ScriptDTO scriptDTO) throws IOException, URISyntaxException {
        HttpResponse response=downloadScriptRequest(fullBitbucketFileUrl);
        if(response!=null){
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String fileName= scriptDTO.getLocalPath();
                scriptsHandler.saveDownloadedScript(entity,fileName);
            }
        }
        else {
            logger.error("can't download script : "+scriptDTO.getName());
        }


    }



}
