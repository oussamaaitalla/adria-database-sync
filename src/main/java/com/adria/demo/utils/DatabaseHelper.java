package com.adria.demo.utils;

import com.adria.demo.configuration.DatabaseConfig;
import com.adria.demo.constants.Constantes;
import com.adria.demo.models.ScriptDTO;
import com.adria.demo.models.SuiviRecord;
import com.adria.demo.models.SyncStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper {
    private Logger logger= LoggerFactory.getLogger(DatabaseHelper.class);

    private DatabaseConfig databaseConfig;

    public DatabaseHelper(DatabaseConfig databaseConfig) {
        this.databaseConfig = databaseConfig;
    }

    public Connection getDatabaseConnection(String url, String username, String password){
        try {
            logger.info("======== Connecting to a database ... ========");
            Connection connection=DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            return null;
        }
    }
    public void closeDatabaseConnection(Connection connection) {
        try {
            logger.info("======== Closing a database connection ... ========");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public String[] extractQueriesFromSqlScript(String scriptPath,String delimiter ) throws IOException {
        String content=new String(Files.readAllBytes(Paths.get(scriptPath)));
        return content.split(delimiter);
    }
    public List<SuiviRecord> getLatestNLoggedScripts(int n, Connection connection, String dbType,char scriptType) {
        List<SuiviRecord> records = new ArrayList<>();
        try {
            logger.info("======== Getting " + n + " Last logged scripts ... ========");
            Statement statement = connection.createStatement();
            String sql = "";

            if(dbType.equals("postgres")) {
                sql = "SELECT * FROM "+ databaseConfig.getDatabase_log_table_name()+" WHERE type='"+scriptType+"' ORDER BY id DESC limit "+n;
            }
            else if(dbType.equals("oracle")) {
                sql = "SELECT * FROM (SELECT * FROM "+databaseConfig.getDatabase_log_table_name()+" WHERE type='"+scriptType+"'ORDER BY id DESC) WHERE rownum <="+n;
            }
            ResultSet resultSet = statement.executeQuery(sql);

            while(resultSet.next()) {
                int id = resultSet.getInt("id");
                String num = resultSet.getString("num");
                String codeBanque = resultSet.getString("code_banque");
                String nom = resultSet.getString("nom");
                String type = resultSet.getString("type");

                String record = "RECORD ==> id="+id+", code_banque="+codeBanque+", type="+type+", num="+num+", nom="+nom;
                System.out.println(record);
                records.add(new SuiviRecord(id, codeBanque, type, num, nom));
            }

            return records;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    public void executeScript(Connection connection, ScriptDTO scriptDTO) throws IOException, SQLException {
        String[] queries = extractQueriesFromSqlScript(scriptDTO.getLocalPath(),";");
        boolean isRaisingException=false;
        for(int i=0; i < queries.length-1; i++){
            try {
                PreparedStatement preparedStatement=connection.prepareStatement(queries[i]);
                preparedStatement.execute();
            }catch (SQLException e){
                //TODO: log the sql state code and the exception to the monitoring databases
                String sqlStateCode=e.getSQLState();
                logger.error("Sql State :"+ sqlStateCode);
                System.out.println(e.getMessage());
                connection.rollback();
                isRaisingException=true;
                break;
            }
        }
        if(!isRaisingException) connection.commit();

    }

    public boolean isDatabaseSynchronized(SyncStatus syncStatus, String codeBanque) {
        if(syncStatus.isDatabaseSync()) {
            logger.info("//////// Current DATA BASE =="+ codeBanque +"== is SYNCHRONIZED !!!");
            return true;
        }
        else {
            logger.warn("//////// Current DATA BASE =="+ codeBanque +"== is NOT SYNCHRONIZED !!!");
            return false;
        }
    }





}
