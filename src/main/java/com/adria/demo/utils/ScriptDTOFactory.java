package com.adria.demo.utils;

import com.adria.demo.models.ScriptDTO;
import com.adria.demo.models.ScriptDataDTO;
import com.adria.demo.models.ScriptStructureDTO;

public class ScriptDTOFactory {
    public ScriptDTO getInstance(String scriptPath){
        String[] splt1 = scriptPath.split("/");
        char scriptType=splt1[1].charAt(0);
        if(scriptType=='D'){
            return new ScriptDataDTO(scriptPath);
        }else return new ScriptStructureDTO(scriptPath);
    }
}
