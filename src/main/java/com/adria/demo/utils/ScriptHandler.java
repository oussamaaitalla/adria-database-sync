package com.adria.demo.utils;

import com.adria.demo.configuration.BitbucketConfig;
import com.adria.demo.configuration.DatabaseConfig;
import com.adria.demo.constants.Constantes;
import com.adria.demo.models.*;
import com.adria.demo.service.PullRequestService;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URISyntaxException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * ScriptHandler is class that defines a set of methods related to any script processing
 */
@Component
public class ScriptHandler {
    private BitbucketConfig bitbucketConfig;
    private PullRequestService pullRequestService;
    private DatabaseConfig databaseConfig;
    private Logger logger= LoggerFactory.getLogger(ScriptHandler.class);

    public ScriptHandler(BitbucketConfig bitbucketConfig, PullRequestService pullRequestService, DatabaseConfig databaseConfig) {
        this.bitbucketConfig = bitbucketConfig;
        this.pullRequestService = pullRequestService;
        this.databaseConfig = databaseConfig;
    }


    /**
     * this method is used to write the content of the downloaded script to a specified path
     * @param entity this is the http response entity that contains the content of the downloaded script
     * @param localPath this is the desired local path where to save the script.
     * @throws IOException
     */
    public void saveDownloadedScript(org.apache.http.HttpEntity entity, String localPath) throws IOException {
        try (FileOutputStream outstream = new FileOutputStream(localPath)) {
            entity.writeTo(outstream);
        }
    }


    /**
     * this is used to construct the api url to extract the script content
     * @param path this is the path of the script in the repository
     * @return String representing the full api url of the script in the repository
     */
    public String constructUrl(String path) {
        return bitbucketConfig.getApi_base_url()+bitbucketConfig.getWorkspace()+"/"+bitbucketConfig.getRepositoryName()+"/src/master/"+path;
    }


    /**
     * this is used to check if the script to be downloaded is already existing in the local path
     * @param scriptDTO this is the script to be checked
     * @return boolean representing the existence of the script
     */
    public boolean isScriptAlreadyDownloaded(ScriptDTO scriptDTO){
        File file=new File(scriptDTO.getLocalPath());
        return file.exists();

    }

    /**
     * this is used to delete all scripts downloaded locally.
     */
    public void deleteDownloadedScripts()  {
        try{
            FileUtils.deleteDirectory(new File(Constantes.SCRIPTS_DIRECTORY_NAME));
            logger.info("--> Scripts has  been deleted");
        }catch (IOException e){
            logger.warn("--> Scripts has not been deleted");
        }

    }


    /**
     * this is used to validate the content conformity of the provided scripts
     * @param prScriptsByDBType this is the scripts to be validated, grouped by the type of database platform.
     * @return List represents the rejected Script that did not pass the content conformity validation.
     * @throws Exception
     */
    public List<RejectedScript>  verifyScriptsContentConformity(Map<String,List<ScriptDTO>> prScriptsByDBType) throws Exception {
        List<ScriptDTO> prScripts=prScriptsByDBType.values().stream()
                                    .flatMap(Collection::stream)
                                    .collect(Collectors.toList());
        List<RejectedScript> rejectedScripts=new ArrayList<>();
        for(ScriptDTO script:prScripts){
            String scriptStatus=script.getStatus();
            if(scriptStatus.equals("added")||scriptStatus.equals("modified")){
                String scriptContent=getScriptContent(script.getLink()).toLowerCase();
                logger.info("--> Verifying ..... "+script.getName());
                if(scriptContent!=null){
                    RejectedScript rejectedScript=getScriptIfRejected(scriptContent,script);
                    if(!rejectedScript.getReasons().isEmpty()){
                        logger.warn("--> "+script.getName()+" is rejected");
                        rejectedScripts.add(rejectedScript);
                    }
                }
                else {
                    logger.error("cannot get content of script : "+script.getName());
                    break;
                }
            }

        }
        return rejectedScripts;

    }


    /**
     * this is used to get the content of a script from the repository
     * @param scriptContentURL this is the api url to get the script content
     * @return String represents script content
     * @throws URISyntaxException
     */
    private String getScriptContent(String scriptContentURL) throws URISyntaxException {
        return pullRequestService.getBodyFromHttpResponse(scriptContentURL);

    }


    /**
     * this is used to check the validation of the script content based on Its type ( strucutre or data)
     * @param scriptContent this is the script content.
     * @param scriptDTO this is the script object that holds the script's information.
     * @return RejectedScript represents an object that contains the rejected script.
     */
    private RejectedScript getScriptIfRejected(String scriptContent, ScriptDTO scriptDTO) {
        if(scriptDTO.getType()=='S'){
            return checkScriptStructureConformity(scriptContent, scriptDTO);
        }
        else{
            return chekScriptDataConformity(scriptContent, scriptDTO);
        }
    }

    /**
     * this is used to validate the conformity of the scripts of type data
     * the validation checks :
     * <ul>
     *     <li>the presence of the log insertion instruction to the adria_suivi_table</li>
     *     <li>the match of the name of the script with the log instruction</li>
     *     <li>the presence of the delete instruction before every insert</li>
     * </ul>
     * @param scriptContent this is the content of the script
     * @param scriptDTO this is the script object
     * @return RejectedScript represents the rejected script that contains the script object and reason of rejection
     */
    private RejectedScript chekScriptDataConformity(String scriptContent, ScriptDTO scriptDTO) {
        RejectedScript rejectedScript=new RejectedScript();
        rejectedScript.setScript(scriptDTO);
        if(!isLogInstructionExists(scriptContent)){
            rejectedScript.getReasons().add(Constantes.MISSING_INSERT_TO_ADRIA_SUIVI_INSTRUCTION);
        }
        if(!isScriptNameMatchesLogInstruction(scriptContent, scriptDTO)) {
            rejectedScript.getReasons().add(Constantes.SCRIPT_NAME_LOG_INSTRUCTION_MISMATCH);
        }
        if(!isDeleteInstructionsExist(scriptContent)) {
            rejectedScript.getReasons().add(Constantes.MISSING_DELETE_BEFORE_INSERT_INSTRUCTION);
        }
        return rejectedScript;
    }

    /**
     * this is used to validate the conformity of the scripts of type data
     * the validation checks :
     * <ul>
     *      <li>the presence of the log insertion instruction to the adria_suivi_table</li>
     *      <li>the match of the name of the script with the log instruction</li>
     * </ul>
     * @param scriptContent this is the content of the script
     * @param scriptDTO this is the script object
     * @return RejectedScript represents the rejected script that contains the script object and reason of rejection
     */
    private RejectedScript checkScriptStructureConformity(String scriptContent, ScriptDTO scriptDTO) {
        RejectedScript rejectedScript=new RejectedScript();
        rejectedScript.setScript(scriptDTO);
        if(!isLogInstructionExists(scriptContent)){
            rejectedScript.getReasons().add(Constantes.MISSING_INSERT_TO_ADRIA_SUIVI_INSTRUCTION);
        }
        if(!isScriptNameMatchesLogInstruction(scriptContent, scriptDTO)) {
            rejectedScript.getReasons().add(Constantes.SCRIPT_NAME_LOG_INSTRUCTION_MISMATCH);
        }
        return rejectedScript;
    }

    /** this is used to check the matching of the name of the script with the log instruction in the script
     * @param scriptContent this is content of the script
     * @param scriptDTO this is script object
     * @return boolean represents the matching if It exists or not
     */
    private boolean isScriptNameMatchesLogInstruction(String scriptContent, ScriptDTO scriptDTO) {
        scriptContent = scriptContent.toLowerCase();

        Pattern regex = Pattern.compile("insert\\s+into\\s+"+databaseConfig.getDatabase_log_table_name()+"\\s*\\(.+\\)");
        Matcher matcher = regex.matcher(scriptContent);
        LogInstructionDTO logInstructionDTO = null;

        if(matcher.find()) {
            String logInstruction = matcher.group();
            logInstructionDTO = new LogInstructionDTO(logInstruction);

        }

        if(logInstructionDTO != null
                && logInstructionDTO.getCode_banque().equals(scriptDTO.getCodeBanque())
                && logInstructionDTO.getScriptNum().equals(scriptDTO.getNum())
                && logInstructionDTO.getScriptType().equals(String.valueOf(scriptDTO.getType()).toLowerCase())
                && logInstructionDTO.getScriptName().equals(scriptDTO.getName().toLowerCase())) {
            return true;
        }
        else return false;
    }

    /**
     * this is used to check the existence of delete instruction before every insert
     * @param scriptContent this is script content
     * @return boolean represents the existence of the delete instruction.
     */
    private boolean isDeleteInstructionsExist(String scriptContent) {
        int deleteMatchersNumber = countMatchers(scriptContent,"delete");
        int insertMatchersNumber = countMatchers(scriptContent,"insert");

        if(deleteMatchersNumber>=(insertMatchersNumber-1)) return true;
        return false;
    }

    private int countMatchers(String scriptContent,String regex) {
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(scriptContent);
        int matcherNumber=0;
        while(matcher.find()) matcherNumber++;
        return matcherNumber;
    }

    /**
     * this is used to check the existence of the log instruction that insert in the adria_suivi_table
     * @param scriptContent this is the script content
     * @return boolean represents the existence of the log instruction
     */
    private boolean isLogInstructionExists(String scriptContent) {
        //TODO : Pas top éviter de mettre des pattern en dur , il faut penser que ces choses peuvent changer à tout moment
        Pattern pattern=Pattern.compile("insert\\s+into\\s+"+databaseConfig.getDatabase_log_table_name());
        Matcher matcher=pattern.matcher(scriptContent);
        return matcher.find();
    }

    /**
     * this is used to get the last merged commit is hash
     * @param url this is the url that contains the last merged commit is hash
     * @return String represents the hash
     * @throws URISyntaxException
     */
    public String getLastMergedCommitHash(String url) throws  URISyntaxException {
        String responseBody=pullRequestService.getBodyFromHttpResponse(url);
        if(responseBody!=null){
            JSONObject jsonBody=new JSONObject(responseBody);
            String commitHash=jsonBody
                    .getJSONArray("values")
                    .getJSONObject(0)
                    .getJSONObject("commit")
                    .getString("hash");
            return commitHash;
        }
        return null;

    }

    /**
     * this is used to convert a list of script to a map that group scripts by database platform
     * @param scriptDTOList this is the list of scripts
     * @return A Map that represents grouped scripts by database platform
     */
    private Map<String, List<ScriptDTO>> splitFilesByDir(List<ScriptDTO> scriptDTOList) {
        Map<String, List<ScriptDTO>> filesByDir = new HashMap<>();
        if(scriptDTOList != null && !scriptDTOList.isEmpty()) {
            for(ScriptDTO scriptDTO: scriptDTOList) {
                if(filesByDir.containsKey(scriptDTO.getRelativeRepoDir())) {
                    filesByDir.get(scriptDTO.getRelativeRepoDir()).add(scriptDTO);
                }
                else {
                    List<ScriptDTO> newScriptDTOList = new ArrayList<>();
                    newScriptDTOList.add(scriptDTO);
                    filesByDir.put(scriptDTO.getRelativeRepoDir(), newScriptDTOList);
                }

            }

            return filesByDir;
        }
        else {
            return null;
        }

    }

    /**
     * this is used to verify the conformity of the script i name with the repository by checking
     * <ul>
     *     <li>the number in the script name is already used or not</li>
     * </ul>
     * @param newScriptsPaths list of scripts to be verified
     * @return A List of rejected script that does not respect the naming rule
     * @throws Exception
     */
    public List<RejectedScript> verifyScriptNamingWithRepo(Map<String, List<ScriptDTO>> newScriptsPaths) throws Exception {
        List<RejectedScript> rejectedScriptList = new ArrayList<>();
        String bitbucketDbScriptsRepoSrcUrl= bitbucketConfig.getApi_base_url()+bitbucketConfig.getWorkspace()+"/"+bitbucketConfig.getRepositoryName()+"/src/";
        String lastMergedCommitHash = getLastMergedCommitHash(bitbucketDbScriptsRepoSrcUrl);
        String scriptsDirectoriesBaseUrl = bitbucketDbScriptsRepoSrcUrl+lastMergedCommitHash+"/";

        List<ScriptDTO> newScriptDTOList;
        if(newScriptsPaths != null) {
            newScriptDTOList = newScriptsPaths.values().stream()
                    .flatMap(Collection::stream)
                    .filter(scriptDTO -> scriptDTO.getStatus().equals("added"))
                    .collect(Collectors.toList());

            Map<String, List<ScriptDTO>> scriptDTOLisByDir = splitFilesByDir(newScriptDTOList);
            if(scriptDTOLisByDir != null) {
                scriptDTOLisByDir.forEach((relativeDir, scriptDTOList) -> {
                    String repoDirUrl = scriptsDirectoriesBaseUrl+relativeDir;
                    try {
                        List<ScriptDTO> scriptDTOListFromRepoDir = getScriptsInRepoDir(repoDirUrl);
                        if(scriptDTOListFromRepoDir != null) {
                            scriptDTOList.forEach(scriptDTO -> {
                                if(isScriptNumUsedInRepo(scriptDTO.getNum(), scriptDTOListFromRepoDir)) {
                                    logger.info("This script number ["+scriptDTO.getRelativeRepoPath()+"] is already used in the remote repo!");
                                    logger.info("The PR will be declined!");

                                    RejectedScript rejectedScript = new RejectedScript();
                                    rejectedScript.setScript(scriptDTO);
                                    rejectedScript.getReasons().add(Constantes.DUPLICATE_SCRIPT_NUMBER_DECLINE_MSG);
                                    rejectedScriptList.add(rejectedScript);
                                }
                                else {
                                    logger.info("This script number ["+scriptDTO.getNum()+"] is not used in the repo.");
                                    logger.info("This Script ["+ scriptDTO.getRelativeRepoPath() +"] is accepted!");
                                }
                            });
                        }
                    } catch (URISyntaxException e) {
                        logger.error(e.getMessage());
                    }
                });

                if(!rejectedScriptList.isEmpty()) {
                    return rejectedScriptList;
                }
                else {
                    logger.info("All the scripts numbers in this PullRequest are Accepted!");
                    return null;
                }
            }
            else return null;
        } else return null;
    }

    /**
     * this is used to verify if the number of the script is already used in the repository or not
     * @param num script number
     * @param scriptDTOListFromRepoDir this is the list of scripts names from the repository
     * @return boolean represents is the number is already taken or not
     */
    private boolean isScriptNumUsedInRepo(String num, List<ScriptDTO> scriptDTOListFromRepoDir) {
        for(ScriptDTO scriptDTO: scriptDTOListFromRepoDir) {
            if(Integer.parseInt(num) == Integer.parseInt(scriptDTO.getNum())) return true;
        }
        return false;
    }

    /**
     * this is used to get script names from a specific directory in the repository
     * @param repoDirUrl the bitbucket repository url
     * @return List of scriptsDTO objects
     * @throws URISyntaxException
     */
    private List<ScriptDTO> getScriptsInRepoDir(String repoDirUrl) throws URISyntaxException {
        List<ScriptDTO> scriptDTOList = new ArrayList<>();

        String responseBody = pullRequestService.getBodyFromHttpResponse(repoDirUrl);
        if(responseBody != null) {
            JSONObject jsonBody = new JSONObject(responseBody);
            JSONArray values = jsonBody.getJSONArray("values");
            for(int i=0; i<values.length(); i++) {
                String path = values.getJSONObject(i).getString("path");
                if(isSqlFile(path)) {
                    ScriptDTOFactory scriptDTOFactory=new ScriptDTOFactory();
                    ScriptDTO scriptDTO = scriptDTOFactory.getInstance(path);
                    scriptDTOList.add(scriptDTO);
                }
            }
            return scriptDTOList;
        }
        else return null;

    }

    /**
     * this is used to check if a script is an sql script or not
     * @param path this is the script path
     * @return boolean represents the type of the script if it sql or not
     */
    private boolean isSqlFile(String path) {
        Pattern pattern = Pattern.compile("\\.sql$");
        Matcher matcher = pattern.matcher(path);
        if(matcher.find()) return true;
        else return false;
    }


    /**
     * this is used to verify the conformity of the pullrequest scripts :
     * <ul>
     *     <li>Verify if scripts are placed in the correct directory based on their types.</li>
     *     <li>Verify if the pullrequest contain only scripts of one type ( structure or data ) </li>
     *     <li>Verify if the scripts of type structure are present for all database platforms</li>
     *     <li>Verify the conformity of the name of the scripts with repository</li>
     *     <li>Verify the scripts content conformity </li>
     * </ul>
     * and launch the declining of the PR if there is a rejcted scripts
     * @param pullRequestDTO this is pullrequestDTO object that holds all the infromation about the puulrequest
     * @throws Exception
     */
    public void  verifyPullRequestScriptsConformity(PullRequestDTO pullRequestDTO) throws Exception {
        Set<RejectedScript> rejectedScriptSet = new HashSet<>();
        Map<String,List<ScriptDTO>> newScriptsPaths=pullRequestDTO.getScriptsByDBType();

        logger.info("Verify if scripts are placed in the correct directory based on their types.");
        List<RejectedScript> rejectedMisPlacedScripts = verifyScriptsPlacement(pullRequestDTO.getAllScripts());
        rejectedScriptSet.addAll(rejectedMisPlacedScripts);

        logger.info("Verify not having both script types in the same PR.");
        List<RejectedScript> rejectedScriptsForBothTypesExistence = verifyScriptsTypesExistence(newScriptsPaths);
        rejectedScriptSet.addAll(rejectedScriptsForBothTypesExistence);

        if(rejectedScriptsForBothTypesExistence.isEmpty()){
            logger.info("Verify if scripts for both platforms oracle and postgres exist.");
            List <RejectedScript> rejectedScriptsFromPlatformExistingVerification= verifyScriptsPlatformExistence(pullRequestDTO.getScriptsType(),newScriptsPaths);
            rejectedScriptSet.addAll(rejectedScriptsFromPlatformExistingVerification);
        }

        logger.info("Verify scripts naming conformity with the repository.");
        List<RejectedScript> rejectedScriptsFromNamingVerification = verifyScriptNamingWithRepo(newScriptsPaths);
        if(rejectedScriptsFromNamingVerification != null) {
            rejectedScriptSet.addAll(rejectedScriptsFromNamingVerification);
        }


        logger.info("Verify conformity of new created scripts.");
        List<RejectedScript> rejectedScriptsFromContentVerification = verifyScriptsContentConformity(newScriptsPaths);
        rejectedScriptSet.addAll(rejectedScriptsFromContentVerification);


        if(!rejectedScriptSet.isEmpty()) {
            pullRequestService.declinePullRequest(rejectedScriptSet, pullRequestDTO.getId());
        }
        else {
            pullRequestService.approvePullRequest(pullRequestDTO.getId());
        }
    }

    /**
     * this is used to verify if the scripts are well placed ( Structure and Data )
     * @param newScriptsPaths this is the scripts to be verified
     * @return A List of rejected scripts
     */
    private List<RejectedScript> verifyScriptsPlacement(List<ScriptDTO> newScriptsPaths) {
        List<RejectedScript> rejectedScripts = new ArrayList<>();

        for(ScriptDTO scriptDTO: newScriptsPaths) {
            if(scriptDTO.isMisplaced()) {
                logger.info("The script ["+scriptDTO.getRelativeRepoPath()+"] is misplaced. REJECTED!");
                RejectedScript rejectedScript = new RejectedScript();
                rejectedScript.setScript(scriptDTO);
                rejectedScript.getReasons().add(Constantes.PR_DECLINE_MSG_MISPLACED_SCRIPT);

                rejectedScripts.add(rejectedScript);
            }
        }

        return rejectedScripts;
    }

    /**
     * this is used to verify that the PR contains only scripts for one type (Strucutre or data )
     * @param newScriptsPaths this is the list of scripts to be verified
     * @return A List of rejected scripts
     */
    private List<RejectedScript> verifyScriptsTypesExistence(Map<String, List<ScriptDTO>> newScriptsPaths) {
        List<RejectedScript> rejectedScripts = new ArrayList<>();

        List<ScriptDTO> scriptDTOList = newScriptsPaths.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        char exitingType = scriptDTOList.get(0).getType();

        for(int i=1; i < scriptDTOList.size(); i++) {
            if(scriptDTOList.get(i).getType() != exitingType) {
                RejectedScript rejectedScript = new RejectedScript();
                rejectedScript.setScript(scriptDTOList.get(i));
                String reason = "The PR should only contain scripts with same type. There is already a script with type ["+exitingType+"]";
                rejectedScript.getReasons().add(reason);
                rejectedScripts.add(rejectedScript);

                logger.info("The script ["+scriptDTOList.get(i).getName()+"] is rejected! The PR can only contains scripts with same type.");
            }
        }

        return rejectedScripts;
    }

    /**
     * this is used to verify that the PR contains structure scripts for all database platform
     * @param scriptsType this is the script type of the scripts in the PR
     * @param newScriptsPaths this is the scripts to be verified
     * @return A List of rejected scripts
     * @throws URISyntaxException
     */
    private List<RejectedScript> verifyScriptsPlatformExistence(String scriptsType,Map<String, List<ScriptDTO>> newScriptsPaths) throws URISyntaxException {
        List<RejectedScript> rejectedScriptList = new ArrayList<>();

        if(!scriptsType.isEmpty() && scriptsType.equals("S")){
            List<List<ScriptDTO>> prScriptsByDBType=new ArrayList<>(newScriptsPaths.values());
            int scriptsSize= prScriptsByDBType.get(0).size();
            int numberOfDatabseTypes=databaseConfig.databasesTypeNumber();
            for (Map.Entry<String, List<ScriptDTO>> entry : newScriptsPaths.entrySet()) {
                List<ScriptDTO> scriptsByDBType = entry.getValue();
                if (prScriptsByDBType.size()!=numberOfDatabseTypes || scriptsSize != scriptsByDBType.size()) {
                    logger.warn(Constantes.PR_IS_MISSING_SCRIPTS_FOR_ALL_DB_PLATFORM);
                    RejectedScript rejectedScript = new RejectedScript();
                    rejectedScript.setScript(null);
                    rejectedScript.getReasons().add(Constantes.PR_IS_MISSING_SCRIPTS_FOR_ALL_DB_PLATFORM);
                    rejectedScriptList.add(rejectedScript);
                    return rejectedScriptList;
                }
            }

        }
        return rejectedScriptList;
    }


    /**
     * this is used to create a directory to hold the downloaded scripts.
     * @param newScriptsPaths this is a list of the downloaded scripts
     */
    public void createDirectoryTree(Map<String, List<ScriptDTO>> newScriptsPaths){
        List<ScriptDTO> scriptDTOs=newScriptsPaths.values()
                .stream().flatMap(Collection::stream)
                .collect(Collectors.toList());
        scriptDTOs.forEach((script)->{
            createPath(Constantes.SCRIPTS_DIRECTORY_NAME+"/"+script.getRelativeRepoDir());
        });
    }

    /**
     * this is used to create directory for a script based on Its location in the repository
     * @param scriptRelativeRepoDir this is the relative location of the script in the repository
     */
    public void createPath(String scriptRelativeRepoDir){
        File directory=new File(scriptRelativeRepoDir);
        if(!directory.exists()) directory.mkdirs();
    }
}
