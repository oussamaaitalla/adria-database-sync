package com.adria.demo.configuration;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "bitbucket")
public class BitbucketConfig {
    private String workspace;
    private String repositoryName;
    private String hookUuid;
    private String api_base_url;
    private String pr_merge_event;
    private String pr_created_event;


    public String getWorkspace() {
        return workspace;
    }


    public String getRepositoryName() {
        return repositoryName;
    }


    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }


    public String getHookUuid() {
        return hookUuid;
    }

    public void setHookUuid(String hookUuid) {
        this.hookUuid = hookUuid;
    }

    public String getApi_base_url() {
        return api_base_url;
    }

    public void setApi_base_url(String api_base_url) {
        this.api_base_url = api_base_url;
    }

    public String getPr_merge_event() {
        return pr_merge_event;
    }

    public void setPr_merge_event(String pr_merge_event) {
        this.pr_merge_event = pr_merge_event;
    }

    public String getPr_created_event() {
        return pr_created_event;
    }

    public void setPr_created_event(String pr_created_event) {
        this.pr_created_event = pr_created_event;
    }
}
