package com.adria.demo.configuration;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
@ConfigurationProperties(prefix = "adria.db.sync")
public class DatabaseConfig {
    List<DataSourceProperties> datasources;
    String database_log_table_name;

    public List<DataSourceProperties> getDatasources() {
        return datasources;
    }

    public void setDatasources(List<DataSourceProperties> datasources) {
        this.datasources = datasources;
    }
    public  int databasesTypeNumber(){
        Set<String> databaseTypes=new HashSet<>();
        for(DataSourceProperties datasource:datasources){
            databaseTypes.add(datasource.getPlatform());
        }
        return databaseTypes.size();
    }

    public String getDatabase_log_table_name() {
        return database_log_table_name;
    }

    public void setDatabase_log_table_name(String database_log_table_name) {
        this.database_log_table_name = database_log_table_name;
    }
}
