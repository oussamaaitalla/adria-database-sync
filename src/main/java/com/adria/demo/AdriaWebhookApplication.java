package com.adria.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdriaWebhookApplication {

    public static void main(String[] args) {

        SpringApplication.run(AdriaWebhookApplication.class, args);

    }


}
