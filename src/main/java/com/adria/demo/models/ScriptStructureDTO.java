package com.adria.demo.models;

import com.adria.demo.constants.Constantes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScriptStructureDTO extends ScriptDTO{
    public ScriptStructureDTO(String scriptPath) {
        super(scriptPath);
        String[] splt1 = scriptPath.split("/");
        this.relativeRepoDir = splt1[0]+"/"+splt1[1]+"/";
        this.name = splt1[2].replaceAll("\\s", "");
        this.num = splt1[2].split("_")[0];
        this.codeBanque = "";
        this.localPath = Constantes.SCRIPTS_DIRECTORY_NAME+"/"+this.relativeRepoDir+this.name;
        this.type='S';
    }


    public boolean isMisplaced() {
        Pattern pattern = Pattern.compile("\\d+_\\w+_\\d+_\\w+\\.sql");
        Matcher matcher = pattern.matcher(this.name.toLowerCase());
        return matcher.find();
    }
}
