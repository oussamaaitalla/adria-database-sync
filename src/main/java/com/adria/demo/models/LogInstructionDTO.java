package com.adria.demo.models;

public class LogInstructionDTO {
    private String code_banque;
    private String scriptNum;
    private String scriptType;
    private String scriptName;

    public LogInstructionDTO(String code_banque, String scriptNum, String scriptType, String scriptName) {
        this.code_banque = code_banque;
        this.scriptNum = scriptNum;
        this.scriptType = scriptType;
        this.scriptName = scriptName;
    }
    public LogInstructionDTO(String logInstruction) {
        logInstruction = logInstruction.toLowerCase();
        String[] splt1 = logInstruction.split("values");
        String st = splt1[1];
        st = st.replace('(', '\0');
        st = st.replace(')',  '\0');

        String[] splt2 = st.split(",");
        if(splt2[1].trim().equals("''")) {
            this.code_banque = "";
        }
        else {
            this.code_banque = splt2[1].replace('\'', '\0').trim();
        }
        this.scriptType = splt2[2].replace('\'', '\0').trim();
        this.scriptNum = splt2[3].replace('\'', '\0').trim();
        this.scriptName = splt2[4].replace('\'', '\0').trim();
    }

    public String getCode_banque() {
        return code_banque;
    }

    public void setCode_banque(String code_banque) {
        this.code_banque = code_banque;
    }

    public String getScriptNum() {
        return scriptNum;
    }

    public void setScriptNum(String scriptNum) {
        this.scriptNum = scriptNum;
    }

    public String getScriptType() {
        return scriptType;
    }

    public void setScriptType(String scriptType) {
        this.scriptType = scriptType;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }
}
