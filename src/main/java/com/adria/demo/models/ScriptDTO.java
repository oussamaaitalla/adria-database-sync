package com.adria.demo.models;

import com.adria.demo.constants.Constantes;

import java.util.Objects;

public abstract class ScriptDTO {
    protected String num;
    protected char type;
    protected String relativeRepoPath;
    protected String localPath;
    protected String name;
    protected String codeBanque;
    protected String platform;
    protected String relativeRepoDir;
    protected String status;
    protected String link;

    public ScriptDTO(String scriptPath) {
        this.status = "";
        this.link = "";
        this.relativeRepoPath = scriptPath;
        String[] splt1 = scriptPath.split("/");
        this.platform = splt1[0].toLowerCase();
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeBanque() {
        return codeBanque;
    }

    public void setCodeBanque(String codeBanque) {
        this.codeBanque = codeBanque;
    }

    public String getRelativeRepoPath() {
        return relativeRepoPath;
    }

    public void setRelativeRepoPath(String relativeRepoPath) {
        this.relativeRepoPath = relativeRepoPath;
    }

    public String getRelativeRepoDir() {
        return relativeRepoDir;
    }

    public void setRelativeRepoDir(String relativeRepoDir) {
        this.relativeRepoDir = relativeRepoDir;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScriptDTO scriptDTO = (ScriptDTO) o;
        return type == scriptDTO.type && num.equals(scriptDTO.num);
    }

    @Override
    public int hashCode() {
        return Objects.hash(num, type);
    }

    public abstract boolean isMisplaced();
}
