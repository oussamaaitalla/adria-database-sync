package com.adria.demo.models;

import com.adria.demo.constants.Constantes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScriptDataDTO extends ScriptDTO{
    public ScriptDataDTO(String scriptPath) {
        super(scriptPath);
        String[] splt1 = scriptPath.split("/");
        this.relativeRepoDir = splt1[0]+"/"+splt1[1]+"/"+splt1[2]+"/";
        this.name = splt1[3].replaceAll("\\s", "");
        this.num = splt1[3].split("_")[0];
        this.codeBanque = splt1[2];
        this.localPath = Constantes.SCRIPTS_DIRECTORY_NAME+"/"+this.relativeRepoDir+this.name;
        this.type='D';
    }

    public boolean isMisplaced() {
        Pattern pattern = Pattern.compile("\\d+_data_\\d+_\\w+\\.sql");
        Matcher matcher = pattern.matcher(this.name.toLowerCase());
        return !matcher.find();
    }
}
