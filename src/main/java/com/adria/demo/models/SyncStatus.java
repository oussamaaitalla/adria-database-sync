package com.adria.demo.models;

import com.adria.demo.constants.Constantes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SyncStatus {
    private boolean databaseSync;
    List<ScriptDTO> notSyncScripts;

    public SyncStatus(boolean databaseSync, List<ScriptDTO> notSyncScripts) {
        this.databaseSync = databaseSync;
        this.notSyncScripts = notSyncScripts;
    }

    public SyncStatus() {
        this.databaseSync = false;
        this.notSyncScripts = new ArrayList<>();

    }

    public boolean isDatabaseSync() {
        return databaseSync;
    }

    public void setDatabaseSync(boolean databaseSync) {
        this.databaseSync = databaseSync;
    }

    public List<ScriptDTO> getNotSyncScripts() {
        return notSyncScripts;
    }

    public void setNotSyncScripts(List<ScriptDTO> notSyncScripts) {
        this.notSyncScripts = notSyncScripts;
    }

    public void checkDatabaseSyncStatus() {
        this.setDatabaseSync(this.getNotSyncScripts().isEmpty());
    }
}