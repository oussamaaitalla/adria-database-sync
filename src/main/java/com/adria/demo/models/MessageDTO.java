package com.adria.demo.models;

import java.util.List;
import java.util.Set;

public class MessageDTO {
    private String message="";

    public MessageDTO(String message) {
        this.message = message;
    }


    public MessageDTO(Set<RejectedScript> rejectedScripts){
        for(RejectedScript rejectedScript:rejectedScripts){
            if(rejectedScript.getScript() != null) {
                message+="## - "+rejectedScript.getScript().getRelativeRepoPath()+"-> [";
            }
            else {
                message += "## - ALL -> [";
            }
            for (String reason : rejectedScript.getReasons()){
                message+=reason+",";
            }
            message+="] ##\n";
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
