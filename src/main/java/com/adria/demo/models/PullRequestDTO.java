package com.adria.demo.models;

import org.json.JSONArray;
import org.json.JSONObject;


import java.util.*;
import java.util.stream.Collectors;

public class PullRequestDTO {

    private int id;
    private String  pullRequestScriptsPathsUrl;
    private Map<String, List<ScriptDTO>> scriptsByDBType;
    private String scriptsType;

    public PullRequestDTO(String pullRequestEventBody){
        JSONObject jsonRequestBody=new JSONObject(pullRequestEventBody);
        this.id=jsonRequestBody.getJSONObject("pullrequest")
                .getInt("id");
        this.pullRequestScriptsPathsUrl=jsonRequestBody.getJSONObject("pullrequest")
                .getJSONObject("links")
                .getJSONObject("diffstat")
                .getString("href");
        this.scriptsByDBType=null;
        this.scriptsByDBType=null;

    }

    public void setPRScriptsType(){
        List<ScriptDTO> scriptDTOs=scriptsByDBType.values()
                .stream().flatMap(Collection::stream)
                .collect(Collectors.toList());
        this.scriptsType=String.valueOf(scriptDTOs.get(0).getType());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPullRequestScriptsPathsUrl() {
        return pullRequestScriptsPathsUrl;
    }

    public void setPullRequestScriptsPathsUrl(String pullRequestScriptsPathsUrl) {
        this.pullRequestScriptsPathsUrl = pullRequestScriptsPathsUrl;
    }

    public Map<String, List<ScriptDTO>> getScriptsByDBType() {
        return scriptsByDBType;
    }

    public List<ScriptDTO> getAllScripts() {
        return this.scriptsByDBType.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public void setScriptsByDBType(Map<String, List<ScriptDTO>> scriptsByDBType) {
        this.scriptsByDBType = scriptsByDBType;
    }

    public String getScriptsType() {
        return scriptsType;
    }

    public void setScriptsType(String scriptsType) {
        this.scriptsType = scriptsType;
    }
}
