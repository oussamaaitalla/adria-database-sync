package com.adria.demo.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RejectedScript {
    private ScriptDTO script;
    private List<String> reasons;

    public RejectedScript() {
        this.reasons=new ArrayList<>();
    }

    public ScriptDTO getScript() {
        return script;
    }

    public void setScript(ScriptDTO script) {
        this.script = script;
    }

    public List<String> getReasons() {
        return reasons;
    }

    public void setReasons(List<String> reasons) {
        this.reasons = reasons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RejectedScript that = (RejectedScript) o;
        if(script.getName().equals(that.script.getName())) {
            that.getReasons().addAll(this.reasons);
            return true;
        }
        else return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(script);
    }
}
