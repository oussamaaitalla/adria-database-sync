package com.adria.demo.models;

public class SuiviRecord {
    private int id;
    private String codeBanque;
    private String type;
    private String num;
    private String nom;

    public SuiviRecord(int id, String codeBanque, String type, String num, String nom) {
        this.id = id;
        this.codeBanque = codeBanque;
        this.type = type;
        this.num = num;
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodeBanque() {
        return codeBanque;
    }

    public void setCodeBanque(String codeBanque) {
        this.codeBanque = codeBanque;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
